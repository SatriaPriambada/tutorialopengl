// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
#include <GL\GLU.h>

// Include GLM
#include <glm/glm.hpp>
#include <GL\GL.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace glm;

#include <common\shader.hpp>
#include <common\vboindexer.hpp>
#include <common\texture.hpp>
#include "common\text2D.hpp"

class myButton {
public:
	void initButton(const char * type , float xleftmost, float xrightmost, float ytop, float ybottom) {
		typeClass = type;
		if (std::strcmp(type, "simple") == 0) {
			programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
		}
		else {
			programID = LoadShaders("TransformVertexShader.vertexshader", "TextureFragmentShader.fragmentshader");
			Texture = loadBMP_alpha(type);

			// Get a handle for our "myTextureSampler" uniform
			TextureID = glGetUniformLocation(programID, "myImageTextureSampler");

			GLfloat g_uv_buffer_data[] = {
				0.0f, 1.0f,
				1.0f, 1.0f,
				1.0f, 0.0f,

				1.0f, 0.0f,
				0.0f, 0.0f,
				0.0f, 1.0f,
			};


			glGenBuffers(1, &uvbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(g_uv_buffer_data), g_uv_buffer_data, GL_STATIC_DRAW);
		}

		GLfloat temp_vertex_buffer_data[] = {
			xleftmost,  ytop, 0.0f,  // Top-left
			xrightmost,  ytop, 0.0f,  // Top-right
			xrightmost, ybottom, 0.0f,  // Bottom-right

			xrightmost, ybottom, 0.0f, // Bottom-right
			xleftmost, ybottom, 0.0f,  // Bottom-left
			xleftmost,  ytop, 0.0f,  // Top-left
		};

		

		glGenBuffers(1, &vertexbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(temp_vertex_buffer_data), temp_vertex_buffer_data, GL_STATIC_DRAW);
		Model = glm::mat4(1.f);

	}

	void drawButton(glm::mat4 View) {
		// Use our shader
		glUseProgram(programID);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform

		GLuint MatrixID = glGetUniformLocation(programID, "MVP");

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, value_ptr(View * Model));

		if (std::strcmp(typeClass, "simple") == 0) {

			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glVertexAttribPointer(
				0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
			);

			// Draw the triangle !
			glDrawArrays(GL_TRIANGLES, 0, 6); // 3 indices starting at 0 -> 1 triangle
		}
		else {

			
			// Bind our texture in Texture Unit 0
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, Texture);
			// Set our "myTextureSampler" sampler to user Texture Unit 0
			glUniform1i(TextureID, 0);

			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glVertexAttribPointer(
				0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
			);

			// 2nd attribute buffer : UVs
			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
			glVertexAttribPointer(
				1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
				2,                                // size : U+V => 2
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
			);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			// Draw the triangle !
			glDrawArrays(GL_TRIANGLES, 0, 6); // 12*3 indices starting at 0 -> 12 triangles
			glDisable(GL_BLEND);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);

		}
	}

	void rotateButton(float angle, glm::vec3 axis) {
		Model = glm::rotate(Model, angle, axis);
	}

	void translateButton(float deltaX, float deltaY, float deltaZ ) {
		Model = glm::translate(Model, glm::vec3(deltaX, deltaY, deltaZ));
	}

	void scaleButton(float deltaX, float deltaY, float deltaZ) {

		Model = glm::scale(Model, glm::vec3(deltaX, deltaY, deltaZ));
	}

	void clearButton() {

		glDeleteBuffers(1, &vertexbuffer);
		glDeleteProgram(programID);
	}
private:
	GLuint programID;
	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint Texture;
	GLuint TextureID;
	const char * typeClass;
	glm::mat4 Model;
	
};