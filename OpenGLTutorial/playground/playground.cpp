﻿//// Include standard headers
//#include <stdio.h>
//#include <stdlib.h>
//#include <vector>
//
//// Include GLEW
//#include <GL/glew.h>
//
//// Include GLFW
//#include <glfw3.h>
//#include <GL\GLU.h>
//GLFWwindow* window;
//
//// Include GLM
//#include <glm/glm.hpp>
//#include "VS2017/test2.cpp"
//#include <GL\GL.h>
//using namespace glm;
//
//#include <common\shader.hpp>
//#include <common\vboindexer.hpp>
//#include <common\texture.hpp>
//#include "common\text2D.hpp"
//#include "playground\myText.cpp"
//#include "playground\myButton.cpp"
//
//const int BYTES_PER_VERTEX = 12;
//const double PI = 3.14159;
//GLuint elementbuffer;
//GLuint normalbuffer;
//GLuint uvbuffer;
//GLuint vertexbufferCircle;
//int size;
//
//void DrawCircle(float cx, float cy, float r, int num_segments)
//{
//	float theta = 2 * 3.1415926 / float(num_segments);
//	float tangetial_factor = tanf(theta);//calculate the tangential factor 
//
//	float radial_factor = cosf(theta);//calculate the radial factor 
//
//	float x = r;//we start at angle = 0 
//
//	float y = 0;
//
//	glBegin(GL_LINE_LOOP);
//	for (int ii = 0; ii < num_segments; ii++)
//	{
//		glVertex2f(x + cx, y + cy);//output vertex 
//
//								   //calculate the tangential vector 
//								   //remember, the radial vector is (x, y) 
//								   //to get the tangential vector we flip those coordinates and negate one of them 
//
//		float tx = -y;
//		float ty = x;
//
//		//add the tangential vector 
//
//		x += tx * tangetial_factor;
//		y += ty * tangetial_factor;
//
//		//correct using the radial factor 
//
//		x *= radial_factor;
//		y *= radial_factor;
//	}
//	glEnd();
//}
//
//void Circle2(float Radius, int Resolution, int center_x, int center_y) {
//	// vectors to hold our data
//	// vertice positions
//	std::vector<glm::vec3> v;
//	// texture map
//	std::vector<glm::vec2> t;
//	// normals
//	std::vector<glm::vec3> n;
//
//	// iniatiate the variable we are going to use
//	float X1, Y1, X2, Y2, Z1, Z2;
//	float inc1, inc2, inc3, inc4, inc5, Radius1, Radius2;
//
//	for (int w = 0; w < Resolution; w++) {
//		for (int h = (-Resolution / 2); h < (Resolution / 2); h++) {
//
//
//			inc1 = (w / (float)Resolution) * 2 * PI;
//			inc2 = ((w + 1) / (float)Resolution) * 2 * PI;
//
//			inc3 = (h / (float)Resolution)*PI;
//			inc4 = ((h + 1) / (float)Resolution)*PI;
//
//
//			X1 = sin(inc1);
//			Y1 = cos(inc1);
//			X2 = sin(inc2);
//			Y2 = cos(inc2);
//
//			// store the upper and lower radius, remember everything is going to be drawn as triangles
//			Radius1 = Radius*cos(inc3);
//			Radius2 = Radius*cos(inc4);
//
//
//
//
//			Z1 = Radius*sin(inc3);
//			Z2 = Radius*sin(inc4);
//
//			// insert the triangle coordinates
//			v.push_back(glm::vec3(Radius1*X1 + center_x, Z1 + center_y, Radius1*Y1));
//			v.push_back(glm::vec3(Radius1*X2 + center_x, Z1 + center_y, Radius1*Y2));
//			v.push_back(glm::vec3(Radius2*X2 + center_x, Z2 + center_y, Radius2*Y2));
//
//
//
//			v.push_back(glm::vec3(Radius1*X1 + center_x, Z1 + center_y, Radius1*Y1));
//			v.push_back(glm::vec3(Radius2*X2 + center_x, Z2 + center_y, Radius2*Y2));
//			v.push_back(glm::vec3(Radius2*X1 + center_x, Z2 + center_y, Radius2*Y1));
//
//
//			// insert the texture map, im using this to draw texture out from a texture atlas,
//			// so you probably want to write your own algorithm for this!
//			t.push_back(glm::vec2(0.6666f, 0.5f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//			t.push_back(glm::vec2(0.666f, 1.0f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//			t.push_back(glm::vec2(1.0f, 1.0f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//
//
//			t.push_back(glm::vec2(0.6666f, 0.5f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//			t.push_back(glm::vec2(1.00f, 1.0f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//			t.push_back(glm::vec2(1.00f, 0.5f)*glm::vec2(1, 0.25f) + glm::vec2(0, 0.25f));
//
//			// insert the normal data
//			n.push_back(glm::vec3(X1, Z1, Y1) / glm::length(glm::vec3(X1, Z1, Y1)));
//			n.push_back(glm::vec3(X2, Z1, Y2) / glm::length(glm::vec3(X2, Z1, Y2)));
//			n.push_back(glm::vec3(X2, Z2, Y2) / glm::length(glm::vec3(X2, Z2, Y2)));
//			n.push_back(glm::vec3(X1, Z1, Y1) / glm::length(glm::vec3(X1, Z1, Y1)));
//			n.push_back(glm::vec3(X2, Z2, Y2) / glm::length(glm::vec3(X2, Z2, Y2)));
//			n.push_back(glm::vec3(X1, Z2, Y1) / glm::length(glm::vec3(X1, Z2, Y1)));
//		}
//
//	}
//
//
//	// finally, create the buffers and bind the data to them
//	std::vector<unsigned short> indices;
//	std::vector<glm::vec3> indexed_vertices;
//	std::vector<glm::vec2> indexed_uvs;
//	std::vector<glm::vec3> indexed_normals;
//	indexVBO(v, t, n, indices, indexed_vertices, indexed_uvs, indexed_normals);
//
//
//	glGenBuffers(1, &vertexbufferCircle);
//	glBindBuffer(GL_ARRAY_BUFFER, vertexbufferCircle);
//	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);
//
//
//	glGenBuffers(1, &uvbuffer);
//	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
//	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);
//
//
//	glGenBuffers(1, &normalbuffer);
//	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
//	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);
//
//	// Generate a buffer for the indices as well 
//	glGenBuffers(1, &elementbuffer);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
//
//
//	// store the number of indices for later use
//	size = indices.size();
//
//	// clean up after us
//	indexed_normals.clear();
//	indexed_uvs.clear();
//	indexed_vertices.clear();
//	indices.clear();
//	n.clear();
//	v.clear();
//	t.clear();
//}
//
//int main(void)
//{
//	// Initialise GLFW
//	if (!glfwInit())
//	{
//		fprintf(stderr, "Failed to initialize GLFW\n");
//		getchar();
//		return -1;
//	}
//	
//
//	glfwWindowHint(GLFW_SAMPLES, 4);
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
//	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//
//	// Open a window and create its OpenGL context
//	window = glfwCreateWindow(1024, 768, "Test OpenGL", NULL, NULL);
//	if (window == NULL) {
//		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
//		getchar();
//		glfwTerminate();
//		return -1;
//	}
//	glfwMakeContextCurrent(window);
//
//	// Initialize GLEW
//	glewExperimental = true; // Needed for core profile
//	if (glewInit() != GLEW_OK) {
//		fprintf(stderr, "Failed to initialize GLEW\n");
//		getchar();
//		glfwTerminate();
//		return -1;
//	}
//
//	// Ensure we can capture the escape key being pressed below
//	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
//
//	// Dark blue background
//	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
//	//// Enable depth test
//	//glEnable(GL_DEPTH_TEST);
//	//// Accept fragment if it closer to the camera than the former one
//	//glDepthFunc(GL_LESS);
//
//	GLuint VertexArrayID;
//	glGenVertexArrays(1, &VertexArrayID);
//	glBindVertexArray(VertexArrayID);
//
//	// Create and compile our GLSL program from the shaders
//	GLuint programID2 = LoadShaders("SimpleVertexShader 2.vertexshader", "SimpleFragmentShader 2.fragmentshader");
//	GLuint programIDTexture = LoadShaders("StandardShading.vertexshader", "StandardShading.fragmentshader");
//
//	static const GLfloat g_vertex_buffer_data[] = {
//		-0.5f,  0.5f, 0.0f,  // Top-left
//		0.5f,  0.5f, 0.0f,  // Top-right
//		0.5f, -0.5f, 0.0f,  // Bottom-right
//
//		0.5f, -0.5f, 0.0f, // Bottom-right
//		-0.5f, -0.5f, 0.0f,  // Bottom-left
//		-0.5f,  0.5f, 0.0f,  // Top-left
//	};
//
//	static const GLfloat second_box[] = {
//
//		-0.25f,  0.75f, 0.0f,  // Top-left
//		0.75f,  0.75f, 0.0f,  // Top-right
//		0.75f, -0.25f, 0.0f,  // Bottom-right
//
//		0.75f, -0.25f, 0.0f, // Bottom-right
//		-0.25f, -0.25f, 0.0f,  // Bottom-left
//		-0.25f,  0.75f, 0.0f,  // Top-left
//	};
//
//	GLfloat vertices[] = { -0.6f, -0.4f, 0.0f, // vertex coordinates
//		0.6f, -0.4f, 0.0f,
//		0.0f, 0.6f, 0.0f };
//	GLfloat colors[] = { 0.1f, 0.4f, 0.8f,     // vertex colors
//		0.1f, 0.4f, 0.8f,
//		0.1f, 0.4f, 0.8f };
//
//	GLuint vertexbuffer2, vertex_buffer_id, color_buffer_id;
//	int number_of_vertices = 3; // Number of vertices in buffer
//	int buffer_size = number_of_vertices * BYTES_PER_VERTEX;
//
//
//	glGenBuffers(1, &vertexbuffer2);
//	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer2);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(second_box), second_box, GL_STATIC_DRAW);
//
//	// Create vertex buffer
//	glGenBuffers(1, &vertex_buffer_id);
//	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
//	glBufferData(GL_ARRAY_BUFFER, buffer_size, vertices, GL_STATIC_DRAW);
//
//	// Create color buffer
//	glGenBuffers(1, &color_buffer_id);
//	glBindBuffer(GL_ARRAY_BUFFER, color_buffer_id);
//	glBufferData(GL_ARRAY_BUFFER, buffer_size, colors, GL_STATIC_DRAW);
//
//	//init text
//	//initText2D("ExportedFont_32.bmp", "bmp");
//
//
//	int Resolution = 100;
//	int Radius = 200;
//	GLuint Texture = loadDDS("uvmap.DDS");
//	GLuint TextureID = glGetUniformLocation(programIDTexture, "myTextureSampler");
//	Circle2(Radius,Resolution, 500, 200);
//
//	myText text1, text2;
//	text1.initText2D("ExportedFont_32.bmp", "bmp");
//	text2.initText2D("Holstein.DDS", "dds");
//
//	myButton button1;
//	button1.initButton("simple", -0.5f, 0.5f, 0.5f, -0.5f);
//
//
//
//	//end draw circle
///****************TEST BALL *******************************/
//	//std::vector<GLfloat> ballVerts;
//
//	//for (int i = 0; i <= 40; i++)
//	//{
//	//	double lat0 = PI * (-0.5 + (double)(i - 1) / 40);
//	//	double z0 = sin(lat0);
//	//	double zr0 = cos(lat0);
//
//	//	double lat1 = PI * (-0.5 + (double)i / 40);
//	//	double z1 = sin(lat1);
//	//	double zr1 = cos(lat1);
//
//	//	for (int j = 0; j <= 40; j++)
//	//	{
//	//		double lng = 2 * PI * (double)(j - 1) / 40;
//	//		double x = cos(lng);
//	//		double y = sin(lng);
//
//	//		ballVerts.push_back(x * zr0); //X
//	//		ballVerts.push_back(y * zr0); //Y
//	//		ballVerts.push_back(z0);      //Z
//
//	//		ballVerts.push_back(0.0f);
//	//		ballVerts.push_back(1.0f);
//	//		ballVerts.push_back(0.0f);
//	//		ballVerts.push_back(1.0f); //R,G,B,A
//
//	//		ballVerts.push_back(x * zr1); //X
//	//		ballVerts.push_back(y * zr1); //Y
//	//		ballVerts.push_back(z1);      //Z
//
//	//		ballVerts.push_back(0.0f);
//	//		ballVerts.push_back(1.0f);
//	//		ballVerts.push_back(0.0f);
//	//		ballVerts.push_back(1.0f); //R,G,B,A
//	//	}
//	//}
//
//	//glGenBuffers(1, &vertexbufferCircle);
//	//glBindBuffer(GL_VERTEX_ARRAY, vertexbufferCircle);
//
//	//GLuint sphereSize = 3200 * 7 * 4; //3200 vertixes * 7 floats
//	//glBufferData(GL_VERTEX_ARRAY, sphereSize, &ballVerts[0], GL_STATIC_DRAW);
//	
//	do {
//
//		// Clear the screen
//		glClear(GL_COLOR_BUFFER_BIT);
//
//		/*Draw a ball
//		*/
//		/*glBindBuffer(GL_VERTEX_ARRAY, vertexbufferCircle);
//		glEnableClientState(GL_VERTEX_ARRAY);
//		glVertexPointer(3, GL_FLOAT, 7 * 4, 0);
//		glEnableClientState(GL_COLOR_ARRAY);
//		glColorPointer(4, GL_FLOAT, 7 * 4, (void*)(3 * 4));
//
//		glDrawArrays(GL_TRIANGLE_STRIP, 0, 3200);
//
//		glBindBuffer(GL_ARRAY_BUFFER, 0);*/
//		// Use our shader
//		button1.drawButton();
//
//		//// Use 2nd shader
//		//glUseProgram(programID2);
//		//// 2nd attribute buffer : vertices
//		//glEnableVertexAttribArray(0);
//		//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer2);
//		//glVertexAttribPointer(
//		//	0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
//		//	3,                  // size
//		//	GL_FLOAT,           // type
//		//	GL_FALSE,           // normalized?
//		//	0,                  // stride
//		//	(void*)0            // array buffer offset
//		//);
//
//		//// Draw the triangle !
//		//glDrawArrays(GL_TRIANGLES, 0, 6); // 6 indices starting at 0 -> 1 box
//
//		//glDisableVertexAttribArray(0);
//
//		//if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
//		//	programID2 = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//
//		//}
//
//
//		//TEXT
//		char text[256];
//		sprintf(text, "Hello sec");
//		text1.printText2D(text, 100, 500, 60);
//
//		
//		sprintf(text, "Hello sec");
//		text2.printText2D(text, 500, 100, 30);
//
//		// Draw sphere using vertex buffer object
//		/*glEnableClientState(GL_VERTEX_ARRAY);
//		glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
//		glVertexPointer(3, GL_FLOAT, 0, NULL);
//		glEnableClientState(GL_COLOR_ARRAY);
//		glBindBuffer(GL_ARRAY_BUFFER, color_buffer_id);
//		glColorPointer(3, GL_FLOAT, 0, NULL);
//		glDrawArrays(GL_TRIANGLES, 0, number_of_vertices);*/
//
//		 //Bind our texture in Texture Unit 0
//		//glActiveTexture(GL_TEXTURE0);
//		//glBindTexture(GL_TEXTURE_2D, Texture);
//		//// Set our "myTextureSampler" sampler to user Texture Unit 0
//		//glUniform1i(TextureID, 0);
//
//		//// 1rst attribute buffer : vertices
//		//glEnableVertexAttribArray(0);
//		////if (vertexbufferCircle != NULL) { printf ("NOTNULL\n"); }
//		//glBindBuffer(GL_ARRAY_BUFFER, vertexbufferCircle);
//		//glVertexAttribPointer(
//		//	0,                  // attribute
//		//	3,                  // size
//		//	GL_FLOAT,           // type
//		//	GL_FALSE,           // normalized?
//		//	0,                  // stride
//		//	(void*)0            // array buffer offset
//		//);
//
//		//// 2nd attribute buffer : UVs
//		//glEnableVertexAttribArray(1);
//		////if (uvbuffer != NULL) { printf ("NOTNULL UV\n"); }
//		//glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
//		//glVertexAttribPointer(
//		//	1,                                // attribute
//		//	2,                                // size
//		//	GL_FLOAT,                         // type
//		//	GL_FALSE,                         // normalized?
//		//	0,                                // stride
//		//	(void*)0                          // array buffer offset
//		//);
//
//		//// 3rd attribute buffer : normals
//		//glEnableVertexAttribArray(2);
//		////if (normalbuffer != NULL) { printf ("NOTNULL normal\n"); }
//		//glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
//		//glVertexAttribPointer(
//		//	2,                                // attribute
//		//	3,                                // size
//		//	GL_FLOAT,                         // type
//		//	GL_FALSE,                         // normalized?
//		//	0,                                // stride
//		//	(void*)0                          // array buffer offset
//		//);
//
//		//// Index buffer
//		////if (normalbuffer != NULL) { printf("NOTNULL element\n"); }
//		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
//
//		//// Draw the triangles !
//		//glDrawElements(
//		//	GL_TRIANGLES,      // mode
//		//	size,    // count
//		//	GL_UNSIGNED_SHORT,   // type
//		//	(void*)0           // element array buffer offset
//		//);
//
//		//glDisableVertexAttribArray(0);
//		//glDisableVertexAttribArray(1);
//		//glDisableVertexAttribArray(2);
//
//		
//		// Swap buffers
//		glfwSwapBuffers(window);
//		glfwPollEvents();
//
//		
//
//
//
//	} // Check if the ESC key was pressed or the window was closed
//	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
//		glfwWindowShouldClose(window) == 0);
//
//	
//	// Cleanup VBO
//	glDeleteBuffers(1, &vertexbuffer2);
//	glDeleteBuffers(1, &vertexbufferCircle);
//	glDeleteBuffers(1, &uvbuffer);
//	glDeleteBuffers(1, &normalbuffer);
//	glDeleteBuffers(1, &elementbuffer);
//	glDeleteVertexArrays(1, &VertexArrayID);
//
//	button1.clearButton();
//	text1.cleanupText2D();
//	text2.cleanupText2D();
//
//
//	// Close OpenGL window and terminate GLFW
//	glfwTerminate();
//	//Testing test;
//	//test.PrintHello();
//	////scanf("%d");
//	return 0;
//}
//
////void drawFilledCircle(GLfloat x, GLfloat y, GLfloat radius) {
////	int i;
////	float PI = 3.1415;
////	int triangleAmount = 20; //# of triangles used to draw circle
////
////							 //GLfloat radius = 0.8f; //radius
////	GLfloat twicePi = 2.0f * PI;
////
////	glBegin(GL_TRIANGLE_FAN);
////	glVertex2f(x, y); // center of circle
////	for (i = 0; i <= triangleAmount; i++) {
////		glVertex2f(
////			x + (radius * cos(i *  twicePi / triangleAmount)),
////			y + (radius * sin(i * twicePi / triangleAmount))
////		);
////	}
////	glEnd();
////}
//// method of drawing circle
////
////Draw a triangle fan.
////Draw a rectangle out of two triangles with an image of a circle texture - mapped onto it.
////Draw a rectangle out of two triangles and use the fragment shader to cut away the fragments that are further away from the center than the circle radius.
////Draw a huge, anti - aliased point.

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "playground\myButton.cpp"
#include "playground\myText.cpp"
using namespace glm;

#include <common/shader.hpp>
#include <common/texture.hpp>

int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Final Task Class making", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	
	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "TextureFragmentShader.fragmentshader" );

	// Get a handle for our "MVP" uniform
	GLuint ProjectionID = glGetUniformLocation(programID, "projectionMatrix");
	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
	// Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(0, 0, 3), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);
	//FOR BUTTON
	myButton button1, button2;
	button1.initButton("radar.bmp", 0, 1, 1, 0);
	button2.initButton("simple", 0, 1, 1, 0);

	button1.rotateButton(45.f, glm::vec3(0,0,2));
	button1.translateButton(1,-1,0);
	button1.scaleButton(2, 2, 1);
	button1.translateButton(-1.2, 2, -2);

	//FOR TEXT
	myText text1, text2;
	text1.initText2D("ExportedFont_32.bmp", "bmp");
	text2.initText2D("Holstein.DDS", "dds");
	do{

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//GET THE PROJECTION
		glUniformMatrix4fv(ProjectionID, 1, GL_FALSE, value_ptr(Projection));
		//IMPORTANT TO DRAW THE TEXT FIRST BECAUSE TEXT HAS NO VIEW MATRIX
		//TEXT
		char text[256];
		sprintf(text, "Hello sec");
		text1.printText2D(text, 100, 500, 60);

		sprintf(text, "Hello sec");
		text2.printText2D(text, 500, 100, 30);

		//DRAW BUTTON
		button1.drawButton(View);
		//button2.drawButton(View);
		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader
	/*glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &TextureID);*/
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

