/*
Simple Gauge drawing example - Sandy Barbour 2005
Combined files - Sandy Barbour 07/12/2009

This is an example of drawing a gauge using OpenGL.
The gauge is drawn in a floating panel so that it can be moved around.
Pressing the F8 key will toggle the display of the window


IMPORTANT NOTES - PLEASE READ
1.
On windows you will need to add these to the link settings, otherwise you will get link errors.
glu32.lib glaux.lib

2.
You will need to download the ExampleGauge images.
These can be found in the library section of the SDK web site.
Unzip the archive to the plugins folder.
Make sure that this creates a folder under plugins called ExampleGauge.
*/

#if IBM
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "XPLMDefs.h"
#include "XPLMDisplay.h"
#include "XPLMDataAccess.h"
#include "XPLMGraphics.h"
#include "XPLMUtilities.h"

/// Handle cross platform differences
#if IBM
#include <gl\gl.h>
#include <gl\glu.h>
#elif LIN
#define TRUE 1
#define FALSE 0
#include <GL/gl.h>
#include <GL/glu.h>
#else
#define TRUE 1
#define FALSE 0
#if __GNUC__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <gl.h>
#include <glu.h>
#endif
#include <string.h>
#include <stdlib.h>
#endif
static XPLMTextureID gTexture[MAX_TEXTURES];
